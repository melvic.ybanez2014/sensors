ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % "2.0.0-RC6",
  "dev.zio" %% "zio-streams" % "2.0.0-RC6"
)

lazy val root = (project in file("."))
  .settings(
    name := "sensors"
  )
