package com.melvic.sensors

sealed trait Measurement {
  def sensorId: SensorId
}

object Measurement {
  final case class Success(sensorId: SensorId, humidity: Integer) extends Measurement
  final case class Failed(sensorId: SensorId) extends Measurement

  sealed trait Record

  object Record {
    final case class Success(sensorId: SensorId, min: Int, avg: Int, max: Int) extends Record
    final case class Failed(sensorId: SensorId) extends Record

    def fail(sensorId: SensorId): Record = Failed(sensorId)

    def show(record: Record): String =
      record match {
        case Failed(sensorId)                 => s"$sensorId,NaN,NaN,NaN"
        case Success(sensorId, min, avg, max) => s"$sensorId,$min,$avg,$max"
      }
  }

  def fromLine(line: String): Option[Measurement] =
    line.split(",").map(_.trim) match {
      case Array()                => None
      case Array(sensorId, "NaN") => Some(Measurement.Failed(sensorId))
      case Array(sensorId, humidity) =>
        humidity.toIntOption.map { value =>
          if (value < 0 || value > 100) Measurement.Failed(sensorId)
          else Measurement.Success(sensorId, value)
        }
      case _ => None
    }

  def isSuccess(measurement: Measurement): Boolean =
    measurement match {
      case Measurement.Success(_, _) => true
      case _                         => false
    }
}
