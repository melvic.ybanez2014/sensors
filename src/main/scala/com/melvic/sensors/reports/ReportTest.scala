package com.melvic.sensors.reports

import com.melvic.sensors.reports.ReportTest.Stats
import com.melvic.sensors.{Measurement, SensorId}
import zio._

import java.io.File

/**
  * This is a naive implementation of [[Report]] that reads the whole files
  * into memory at once
  */
case class ReportTest() extends Report {
  override def fromFiles(files: List[File]) = {
    val fileCount = files.length
    val measurementsByFile = files.map(listMeasurements)
    val measurements = measurementsByFile.flatten
    val measurementCount = measurements.length
    val failedMeasurementCount = measurements.count(!Measurement.isSuccess(_))
    val humidityMap: Map[SensorId, Measurement.Record] =
      measurements.groupBy(_.sensorId).map {
        case (sensorId, ms) =>
          val successMs = ms.filter(Measurement.isSuccess)
          val count = successMs.length

          if (count == 0)
            (sensorId, Measurement.Record.Failed(sensorId))
          else {
            val min = successMs.foldLeft(Int.MaxValue) {
              case (min, Measurement.Success(_, humidity)) =>
                if (humidity < min) humidity else min
            }

            val max = successMs.foldLeft(Int.MinValue) {
              case (max, Measurement.Success(_, humidity)) =>
                if (humidity > max) humidity else max
            }

            val sum = successMs.foldLeft(0.0) {
              case (sum, Measurement.Success(_, humidity)) => sum + humidity
            }
            val avg = sum / count

            (sensorId, Measurement.Record.Success(sensorId, min, avg.toInt, max))
          }
      }
    val stats = Stats(
      fileCount,
      measurementCount,
      failedMeasurementCount,
      humidityMap.values.toList.sortWith {
        case (s, Measurement.Record.Failed(_))                                      => true
        case (Measurement.Record.Failed(_), Measurement.Record.Success(_, _, _, _)) => false
        case (Measurement.Record.Success(_, _, avg1, _), Measurement.Record.Success(_, _, avg2, _)) =>
          avg1 > avg2
      }
    )
    Console.printLine(Stats.show(stats))
  }

  def listMeasurements(file: File): List[Measurement] = {
    val source = io.Source.fromFile(file)
    val stream = source.getLines
    stream.next // assumes the first line is the header, and ignores it
    val measurements = stream.foldLeft(List.empty[Measurement]) { (acc, line) =>
      Measurement.fromLine(line).map(_ :: acc).getOrElse(acc)
    }
    source.close
    measurements
  }
}

object ReportTest {
  final case class Stats(
      fileCount: Int,
      measurementCount: Int,
      measurementFailed: Int,
      humidity: List[Measurement.Record]
  )

  object Stats {
    def show: Stats => String = {
      case Stats(fileCount, measurementCount, measurementFailed, humidity) =>
        s"""Number of processed files: $fileCount
           |Number of processed measurements: $measurementCount
           |Number of failed measurements: $measurementFailed
           |
           |Sensors with highest avg humidity:
           |
           |sensor-id,min,avg,max
           |${humidity.map(Measurement.Record.show).mkString("\n")}
           |""".stripMargin
    }
  }

  val layer: ULayer[Report] = ZLayer.succeed(ReportTest())
}
