package com.melvic.sensors.reports

import zio.{RIO, Task, ZIO}

import java.io.File

trait Report {
  def fromFiles(files: List[File]): Task[Unit]
}

object Report {
  def fromDir(dirPath: String): RIO[Report, Unit] = {
    val dir = new File(dirPath)
    val files =
      if (dir.exists && dir.isDirectory) dir.listFiles.filter(_.isFile)
      else Array.empty[File]
    fromFiles(files.toList)
  }

  def fromFiles(files: List[File]): RIO[Report, Unit] = ZIO.serviceWithZIO(_.fromFiles(files))
}
