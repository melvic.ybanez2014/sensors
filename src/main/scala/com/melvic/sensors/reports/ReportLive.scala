package com.melvic.sensors.reports

import com.melvic.sensors.Measurement
import com.melvic.sensors.reports.ReportLive.Stats
import zio._
import zio.stream.ZStream

import java.io.File
import scala.io.Source

final case class ReportLive() extends Report {
  override def fromFiles(files: List[File]) = {
    val fileCount = files.length
    val measurementsByFile = files.map(fromFile)
    val init = ZStream.fromChunk(Chunk.empty[Either[Throwable, Measurement]])
    val measurements = measurementsByFile.foldLeft(ZStream.absolve(init))(_.concat(_))
    val measurementCountEffect = measurements.runCount
    val failedMeasurementCountEffect = measurements.filter(!Measurement.isSuccess(_)).runCount
    val humidityMap = measurements.groupByKey(_.sensorId) {
      case (sensorId, stream) =>
        val chunksEffect: Task[Measurement.Record] = stream.runCollect.map { chunk =>
          val successMs = chunk.filter(Measurement.isSuccess)
          val count = successMs.size
          if (count == 0)
            Measurement.Record.Failed(sensorId)
          else {
            val min = successMs.foldLeft(Int.MaxValue) {
              case (min, Measurement.Success(_, humidity)) =>
                if (humidity < min) humidity else min
            }

            val max = successMs.foldLeft(Int.MinValue) {
              case (max, Measurement.Success(_, humidity)) =>
                if (humidity > max) humidity else max
            }

            val sum = successMs.foldLeft(0.0) {
              case (sum, Measurement.Success(_, humidity)) => sum + humidity
            }

            Measurement.Record.Success(sensorId, min, (sum / count).toInt, max)
          }
        }
        ZStream.fromZIO(chunksEffect)
    }

    for {
      measurementCount <- measurementCountEffect
      failedMeasurementCount <- failedMeasurementCountEffect
      stats = Stats(
        fileCount,
        measurementCount,
        failedMeasurementCount,
        humidityMap
      )
      _ <- Stats.show(stats)
    } yield ()
  }

  def fromFile(file: File): ZStream[Any, Throwable, Measurement] =
    ZStream
      .acquireReleaseWith(ZIO.attempt(Source.fromFile(file)))(source => ZIO.succeed(source.close()))
      .flatMap(source => ZStream.fromIterator(source.getLines))
      .map(line => Measurement.fromLine(line))
      .filter(_.nonEmpty)
      .map(_.get)
}

object ReportLive {
  final case class Stats(
      fileCount: Int,
      measurementCount: Long,
      failedMeasurementCount: Long,
      humidity: ZStream[Any, Throwable, Measurement.Record]
  )

  object Stats {
    def show: Stats => Task[Unit] = {
      case Stats(fileCount, measurementCount, measurementFailed, humidity) =>
        // We are sorting only the top records. There is no support to
        // sort streams using ZStream
        val topSize = 50
        val top = humidity.take(topSize).runCollect.map { chunk =>
          chunk.sortWith {
            case (s, Measurement.Record.Failed(_))                                      => true
            case (Measurement.Record.Failed(_), Measurement.Record.Success(_, _, _, _)) => false
            case (Measurement.Record.Success(_, _, avg1, _), Measurement.Record.Success(_, _, avg2, _)) =>
              avg1 > avg2
          }
        }

        val rest = humidity.drop(topSize)

        for {
          _ <- Console.printLine(
            s"""Number of processed files: $fileCount
            |Number of processed measurements: $measurementCount
            |Number of failed measurements: $measurementFailed
            |
            |Sensors with highest avg humidity:
            |
            |sensor-id,min,avg,max""".stripMargin
          )
          _ <- top.flatMap(chunk => Console.printLine(chunk.map(Measurement.Record.show).mkString("\n")))
          _ <- rest.foreach(record => Console.printLine(Measurement.Record.show(record)))
        } yield ()
    }
  }

  val layer: ULayer[Report] = ZLayer.succeed(ReportLive())
}
