package com.melvic.sensors

import com.melvic.sensors.reports.{Report, ReportLive}
import zio._

object Main extends zio.ZIOAppDefault {
  def run = getArgs.flatMap { args =>
    if (args.length != 1) Console.printLine("Invalid path")
    else
      Report.fromDir(args.head).provide(ReportLive.layer)
  }
}
